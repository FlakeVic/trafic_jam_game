import pygame
from random import choice
from pygame.locals import *

from model_car import Car, CarPoints, CarDirection, START_POINTS_AND_DIRECTION
from config import SCREEN_SIZE, MAX_CARS


class TrafficJamApp:
    def __init__(self, cell_size: int = 15, speed: int = 60):
        self.width = SCREEN_SIZE[0]
        self.height = SCREEN_SIZE[1]
        self.cell_size = cell_size

        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        print(type(self.screen))

        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size

        self.speed = speed

        self.car_size = 30
        self.cars = []

    def draw_road(self):
        dif = -60
        for i in range(3):
            if i != 1:
                pygame.draw.line(
                    self.screen,
                    pygame.Color('Black'),
                    (0, self.height // 2 + i * -dif + dif),
                    (self.width // 2 + dif, self.height // 2 + i * -dif + dif)
                )
                pygame.draw.line(
                    self.screen,
                    pygame.Color('Black'),
                    (self.width // 2 - dif, self.height // 2 + i * -dif + dif),
                    (self.width, self.height // 2 + i * -dif + dif)
                )
            else:
                for x in range(0, self.width + 40, 40):
                    if x == self.width // 2 - 10:
                        continue
                    pygame.draw.line(
                        self.screen,
                        pygame.Color('Black'),
                        (x, self.height // 2 + i * -dif + dif),
                        (x + 20, self.height // 2 + i * -dif + dif)
                    )
        for i in range(3):
            if i != 1:
                pygame.draw.line(
                    self.screen,
                    pygame.Color('Black'),
                    (self.width // 2 + i * -dif + dif, 0),
                    (self.width // 2 + i * -dif + dif, self.height // 2 + dif)
                )
                pygame.draw.line(
                    self.screen,
                    pygame.Color('Black'),
                    (self.width // 2 + i * -dif + dif, self.height // 2 - dif),
                    (self.width // 2 + i * -dif + dif, self.height)
                )
            else:
                for y in range(0, self.height + 40, 40):
                    if y != self.height // 2 - 10:
                        pygame.draw.line(
                            self.screen,
                            pygame.Color('Black'),
                            (self.height // 2 + i * -dif + dif, y),
                            (self.height // 2 + i * -dif + dif, y + 20)
                        )

    def run(self):
        pygame.init()
        clock = pygame.time.Clock()
        running = True
        self.create_car()
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
            self.screen.fill(pygame.Color('White'))
            self.draw_road()
            for car in self.cars:
                if not car.deleting_candidate:
                    car.move()
                    car.check_to_delete()
                    if car.deleting_candidate:
                        self.cars.remove(car)
                car.draw()

            fl = True
            while len(self.cars) < MAX_CARS and fl:
                available_points = self.where_the_car_may_spawn()
                if available_points:
                    start_point = choice(available_points)
                    print(start_point)
                    self.create_car(start_point=start_point)
                else:
                    fl = False

            pygame.display.flip()
            clock.tick(self.speed)
        pygame.quit()

    def where_the_car_may_spawn(self):
        existed_point = []
        for car in self.cars:
            r = car.rect
            rc = [r.topleft, r.bottomleft, r.topright, r.bottomright]

            for key, value in START_POINTS_AND_DIRECTION.items():
                start_x, start_y, dir = value
                for x, y in rc:
                    dif_x = 60 if dir in [CarDirection.left, CarDirection.right] else 40
                    dif_y = 40 if dif_x == 60 else 60
                    if start_x < x < start_x + dif_x or start_y < y < start_y + dif_y:
                        existed_point.append(key)

        available_points = []
        for key in START_POINTS_AND_DIRECTION:
            if key not in existed_point:
                available_points.append(key)
        return available_points

    def create_car(self, start_point: CarPoints = CarPoints.left, end_point: CarPoints = CarPoints.right):
        self.cars.append(
            Car(
                start_point,
                end_point,
                self.screen
            )
        )
