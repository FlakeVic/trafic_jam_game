from random import choice, randint

import pygame
from pygame.locals import Rect
from pygame.surface import Surface
from config import *

Colors = ['Green', 'Yellow', 'Red', 'Blue']


class CarDirection:
    left = 'left'
    right = 'right'
    top = 'top'
    bottom = 'bottom'


class CarPoints:
    left = 'left'
    right = 'right'
    top = 'top'
    bottom = 'bottom'


START_POINTS_AND_DIRECTION = {
    CarPoints.left: (0, HEIGHT // 2 + 10, CarDirection.right),
    CarPoints.right: (WIDTH - 70, HEIGHT // 2 - 50, CarDirection.left),
    CarPoints.top: (WIDTH // 2 - 50, 1, CarDirection.bottom),
    CarPoints.bottom: (WIDTH // 2 + 10, HEIGHT - 70, CarDirection.top)
}


class Car:
    def __init__(self, start_point: CarPoints, finish_point: CarPoints, screen: Surface):
        self.color = choice(Colors)
        self.speed = 5  # randint(1, 5)
        self.start_point = start_point
        self.finish_point = finish_point
        self.screen = screen
        self.x_point, self.y_point, self.direction = START_POINTS_AND_DIRECTION[start_point]
        self.width = 40
        self.length = 60
        self.rect = self._create_car_rect()
        self.deleting_candidate = False

    def _create_car_rect(self):
        if self.direction in [CarDirection.top, CarDirection.bottom]:
            rect = pygame.draw.rect(
                self.screen,
                pygame.Color(self.color),
                Rect(self.x_point, self.y_point, self.width, self.length)
            )
        if self.direction in [CarDirection.left, CarDirection.right]:
            rect = pygame.draw.rect(
                self.screen,
                pygame.Color(self.color),
                Rect(self.x_point, self.y_point, self.length, self.width)
            )
        return rect

    def draw(self):
        pygame.draw.rect(
            self.screen,
            pygame.Color(self.color),
            self.rect
        )

    def move(self):
        match self.direction:
            case CarDirection.right:
                self.rect.move_ip((self.speed, 0))
            case CarDirection.left:
                self.rect.move_ip((-self.speed, 0))
            case CarDirection.top:
                self.rect.move_ip((0, -self.speed))
            case CarDirection.bottom:
                self.rect.move_ip((0, self.speed))

    def check_to_delete(self):
        r = self.rect
        rect_coordinates = [r.topleft, r.bottomleft, r.topright, r.bottomright]

        for x, y in rect_coordinates:
            if 0 < x < SCREEN_SIZE[0] and 0 < y < SCREEN_SIZE[1]:
                self.deleting_candidate = False
                break
        else:
            self.deleting_candidate = True
